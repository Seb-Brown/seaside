<?php
	function array_random($arr, $num = 1) {
		shuffle($arr);
		
		$r = array();
		for ($i = 0; $i < $num; $i++ ){
			$r[] = $arr[$i];
		}
		return $num == 1 ? $r[0] : $r;
	}
	
	$a = array(
		"Love what Seaside is doing? You can tip for free here: https://loots.com/seaside",
		"Follow Seaside on Twitter! https://twitter.com/seasidecs",
		"Subscribing is totally optional! For $4.99/month you get no ads, a special chat badge, custom emotes on ALL of Twitch! Subscribe here: https://goo.gl/mq3zF8",
		"Get 10% off Into the AM by using code SEA10 at checkout! https://www.intotheam.com/",
        "Get 10% off Gamer Supps by using code SEA10 at checkout! https://gamersupps.gg/",
		"If you love what Seaside is doing you can show your support and donate here: https://streamlabs.com/seaside",
		"Be brave and support the stream! www.brave.com/sea218",
		"You can buy Seaside merch on TeeSpring: https://teespring.com/stores/seaside",
		"Support the stream with a FREE SUBSCRIPTION! Click subscribe now to check if a free prime sub is available to use!"
	);
	
	print_r(array_random($a));
?>